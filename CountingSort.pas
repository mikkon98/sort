unit CountingSort;

uses TypeDef;

procedure Counting(var arr: AI; n, x: integer);

const
  MILLISEC_IN_SEC = 1000;
var
  start, finish: integer;
  i, j, pos, cur: integer;
  bufArr: array[0..size] of integer;

begin
  CompCount:=0;
  SwapCount:=0;
  start := Milliseconds();
  for i := 1 to n do
  begin
    SwapCount+=1;
    cur := arr[i];
    bufArr[cur] := bufArr[cur] + 1;
  end;
  pos := 0;
  
  for i := 0 to x do
  begin
  CompCount+=1;
  if(bufArr[i] > 0) then
      for j := 1 to bufArr[i] do
      begin
        SwapCount+=1;
        inc(pos);
        arr[pos] := i;
      end;
  end;
  finish := Milliseconds();
  MassOut(arr,n);
  writeln('Количество сравнений: ',CompCount);
  writeln('Количество перестановок: ',SwapCount);
  writeln('Затрачено ', (finish - start) / MILLISEC_IN_SEC, ' секунд' );
end;
end.